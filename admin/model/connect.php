<?php
//Подключение к базе по PDO
require_once 'dbvar.php';
$dbconstring=$dbtype.":host=".$dbhost.";dbname=".$dbname.";charset=".$dbcharset;  //Строка подключения
$opt = array(  
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC 
); //Массив п
//echo $dbconstring;
$connect= new PDO($dbconstring, $dbuser, $dbpass, $opt);
$connect->exec("SET NAMES utf8 COLLATE utf8_unicode_ci; SET CHARACTER SET utf8");

/* 
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */

