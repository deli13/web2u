<?php
class dbquery {
    private $table; //таблица
    private $connect; //PDO соединение
    
    public function __construct($connect, $table) {
        $this->table=$table;
        $this->connect=$connect;
    }
    private function ecranarray($field){  //экранирование для запросов
        $field=implode("', '", $field);
        $field="'".$field."'";
        return $field;
    }
    
    public function insert($field, $values){ //метод для вставки в таблицу $field поля, $values значения
        if (is_array($field)){
            $field=implode(', ',$field);
        }
        if (is_array($values)){
            $values=$this->ecranarray($values);
        } else {
            $values="'".$values."'";
        }
        $query="insert into ".$this->table." (".$field.") values (".$values.")";
        //echo $query.'\n';
        $this->connect->query($query);
        return $this->connect->lastInsertId();
    }
    
    public function update($field, $value, $where){  //Обновление данных
        if (is_array($field)){
            $str='';
            for ($i=0; $i<count($field); $i++){
                $str.=$field[$i]." = "."'".$value[$i]."'";
                if ($i<count($field)-1){
                    $str=$str.', ';
                }
            }
        } else{
            $str=$field." = '".$value."'";
        }
        $query="update ".$this->table." set ".$str." where ".$where;
        //echo $query;
        $this->connect->query($query);
    }
    
    public function delete($where) { //удаление кортежа
        $query='delete from '.$this->table.' where '.$where;
        //echo $query;
        $this->connect->query($query);
    }
    
    public function select($where) { //выборка по условию
        if ($where!='') $where=' where '.$where;
        $query='select * from '.$this->table.''.$where;
        $result=$this->connect->query($query);
        $row=$result->fetchAll();
        return $row;
    }
    
    public function selectColumn($a) { //Выборка определённых полей
        $query='select '.$a.' from '.$this->table; 
        $result=$this->connect->query($query);
        $row=$result->fetchAll();
        return $row;
    }
    
    public function selectJoin() { //Выборка из 2-х связанных таблиц, имена foreign_key должны быть одинаковыми!!! Пиздец забористый класс
        $table=  func_get_arg(0);
        $id= func_get_arg(1);
        $column='*';
        if (func_num_args()==4){
            $coulumn=func_get_arg(3);
        }

        $join=function($parent,$tab,$id){ //Лямбда Хуямбда
            $str='';
            for ($i=0; $i<count($tab); $i++){
                $str.=' left join '.$tab[$i].' on '.$parent.'.'.$id[$i].'='.$tab[$i].'.'.$id[$i];
            }
            return $str;
        };
        if (is_array($table)){ //Создание запроса с джоином для нескольких таблиц
            $query='select '.$column.' from '.$this->table.' '.$join($this->table, $table, $id);
        } else {
            $query='select '.$column.' from '.$this->table.' left join '.$table.' on '.$this->table.'.'.$id.'='.$table.'.'.$id;
        }
        if (func_num_args()>=3){  //Если передаётся третий параметр то добавляется условие
            $where= func_get_arg(2);
            $query.=' where '.$where;
        }
        //echo $query;
        $result=$this->connect->query($query);
        $row=$result->fetchAll();
        return $row;
    }
    
    public function query($sql){ //Произвольный запрос
        $result=$this->connect->query($sql);
        $row=$result->fetchAll();
        return $row;
    }
}
