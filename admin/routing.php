<?php
?>
<html>
    <head>
        <meta charset="utf8">
        <title>Маршрутизация</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>tinymce.init({ 
                selector:'textarea',
                plugins: ['code autolink link image table']
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$index="Location: /admin/index.php";
if ($_SESSION["role"]!="admin"){
    header($index);
}
$table="t_template";
$conn=new dbquery($connect, $table);
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
        <div class="col-md-10">
            <div class="table_div">
            <table class="table">
                <thead>
                <th>Путь</th>
                <th>Шаблон</th>
                <th>Изменить</th>
                </thead>
                <tbody>
                
                </tbody>
            </table>
            </div>
            <div class="form-group">
                <blockquote>Путь должен начинаться со "/", звёздочкой "*" обозначается любое имя на втором уровне вложенности.<br/>
                Пример /name и /name/*</blockquote>
                
                <form action="/admin/controller/routing_controller.php" method="POST">
                    <input name="url" class="form-control" placeholder="Введите путь"><br/>
                    <select name="path" class="form-control">
                        <option
                    <?php $query=$conn->select('');
                    foreach ($query as $row) {
                        echo "<option value='".$row['path_template']."'>".$row['name_template']."</option>";
                    }
                    unset($conn);
                    ?>
                    </select><br>
                    <input type="submit" name="new" class="form-control btn-primary" value="Сохранить">
                </form>
            </div>
        </div>
        </div>
        <script>
        window.onload=function(){
            $.ajax({
                type: "post",
                url: "/admin/controller/routing_controller.php",
                dataType: "json",
                data: "select",
                success: function(data){
                   var result=$.parseJSON=data;
                   $.each(result, function(key,value){
                      $("tbody").append($("<tr>"));
                      $("tbody").append($("<td>"+key+"</td><td>"+value+"</td>"));
                      $("tbody").append($("<td><form name='delete' action='/admin/controller/routing_controller.php' method='POST'><input name='id' value='"+key+"' style='display:none'><input type='submit' class='btn btn-danger' name='delete' value='Удалить'></form></td>"))
                      $("tbody").append($("</tr>"));
                  });
                }
            })
        }
        
                $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        </script>
    </body>
</html>