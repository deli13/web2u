<?php ?>
<html>
    <head>
        <meta charset="utf8">
        <title>Раздел CMS</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Lightbox -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css" />
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();

$table='t_brand';

?>
    <body>
            <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <div class="table_div">
                    <table class="table">
                        <thead>
                            <th>ID</th>
                            <th>Имя</th>
                            <th>Описание</th>
                            <th>Изображение</th>
                            <th>Создано</th>
                            <th>Изменено</th>
                            <th>Действие</th>
                        </thead>
                        <tbody>
                            <?php 
                            $conn=new dbquery($connect, $table);
                            $result=$conn->select("");
                            foreach ($result as $row){
                                echo "<tr>";
                                echo "<td>{$row['id_brand']}</td>";
                                echo "<td>{$row['name_brand']}</td>";
                                echo "<td>".str200($row['description_brand'])."</td>";
                                echo "<td><img src='{$row['img_brand']}' height='70'></td>";
                                echo "<td>".dateNorm($row['created_brand'])."</td>";
                                echo "<td>".dateNorm($row['updated_brand'])."</td>";
                                echo "<td><form method='POST' name='delete'  action='/admin/controller/brand_controller.php'><input name='id' value='{$row['id_brand']}' style='display:none'>"
                                ." <input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                ." <form method='POST' name='update' action='/admin/controller/brand_controller.php'><input name='id' value='{$row['id_brand']}' style='display:none'>"
                                ." <input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <form method="POST" action="/admin/controller/brand_controller.php" enctype="multipart/form-data">
                        <input class="form-control" type="text" name="id" id="id" style="display: none"><br>
                    <input class="form-control" type="text" name="name" id="name" placeholder="Название бренда"><br>
                    <textarea class="form-control" rows="15" name="description" id="description" placeholder="Описание"></textarea><br>
                    <input type="file" accept="image/*" name="file"><img src="" id="img"><br>
                    <input class="form-control" type="text" name="title" id="title" placeholder="title"><br>
                    <input class="form-control" type="text" name="descr_seo" id="descr_seo" placeholder="description"><br>
                    <input class="form-control" type="text" name="keywords" id="keywords" placeholder="keywords"><br>
                    <input type="submit" class="form-control btn-primary" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
        <script>
                $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/brand_controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                        $('#id').val(req['id']);
                        $('#name').val(req['name']);
                        $('#description').text(req['descr']);
                        $('#img').attr("src", req['img']);
                        $('#img').attr("height", '200');
                        $('#title').val(req['title']);
                        $('#descr_seo').val(req['descr_seo']);
                        $('#keywords').val(req['keywords']);
                    tinyMCE.activeEditor.setContent(req['descr']);
                }
            })
            return false})
            
            $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        </script>
    </body>
</html>
