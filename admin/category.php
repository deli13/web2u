<?php
?>
<html>
    <head>
        <meta charset="utf8">
        <title>Категории</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
// $index="Location: /admin/index.php";
// if ($_SESSION["role"]!="admin"){
    // header($index);
// }
$table_name='t_category';
$table_templ='t_template';
//страница работы с шаблонами
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
        
        <div class="col-md-10">
            <div class="table_div">
            <table class="table">
                <thead>
                <th>ID</th>
                <th>Имя</th>
                <th>Описание категории</th>
                <th>Имя Шаблона</th>
                <th>Создано</th>
                <th>Изменено</th>
                <th>Действие</th>
                </thead>
                <tbody>
                    <?php 
                    $conn=new dbquery($connect,$table_name);
                    $result=$conn->selectJoin($table_templ, 'id_template');
                    foreach ($result as $row){
                        echo "<tr>";
                        echo "<td>{$row["id_category"]}</td>";
                        echo "<td>{$row["name_category"]}</td>";
                        echo "<td>".str200($row["description_category"])."</td>";
                        echo "<td>{$row["name_template"]}</td>";
                        echo "<td>".dateNorm($row['created_category'])."</td>";
                        echo "<td>".dateNorm($row['updated_category'])."</td>";
                        echo "<td><form method='POST' name='delete' action='/admin/controller/category_controller.php'>" //Форма удаления
                        ."<input name='id' value={$row['id_category']} style='display:none'>"
                        ."<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                        ."<form name='update'>" //Форма удаления
                        ."<input name='id' value={$row['id_category']} style='display:none'>"
                        ."<input type='submit' name='update' class='btn btn-success' value='Изменить'></form>"
                        . "</td>";
                        echo '</tr>';
                    }
                    unset($conn);
                    ?>
                    
                </tbody>
            </table>
            </div>
            <div class="form-group">
            <form action="/admin/controller/category_controller.php" method="POST">
                <input type="text" name="id" id="id" style="display: none">
                <input class="form-control" type="text" name="name" id="name" placeholder="Имя категории"><br />
                <select class="form-control" id="id_templated" required name="id_template">
                    <option selected disabled>Выберите шаблон</option>
                    <?php 
                    $conn=new dbquery($connect, $table_templ);
                    $query=$conn->selectColumn('id_template, name_template');
                    foreach ($query as $row) {
                        echo "<option value='".$row['id_template']."'>".$row['name_template']."</option>";
                    }
                    unset($conn);
                    ?>
                </select><br />
                <textarea class="form-control" rows="15" id="description" name="description" placeholder="Описание категории"></textarea><br />
                <input class="form-control" type="text" name="title" id="title" placeholder="title"><br>
                <input class="form-control" type="text" name="descr_seo" id="descr_seo" placeholder="description"><br>
                <input class="form-control" type="text" name="keywords" id="keywords" placeholder="keywords"><br>
                <input class="form-control btn-primary" type="submit" value="Сохранить">
            </form>
            </div>
        </div>
        </div>
        <script>
        $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/category_controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                    $('#id').val(req['id']);
                    $('#name').val(req['name']);
                    $('#id_templated [value="'+req['id_temp']+'"]').attr("selected", "selected");
                    $('#description').text(req['description']);
                    $('#title').val(req['title']);
                    $('#descr_seo').val(req['descr_seo']);
                    $('#keywords').val(req['keywords']);
                    tinyMCE.activeEditor.setContent(req['description']);
                    
                }
            })
        return false})
        
                $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        </script>
    </body>
</html>
