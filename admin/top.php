<?php 
if ((!isset($_SESSION['id'])) || (!isset($_SESSION['login']))){
    session_destroy();
    header('Location: /admin/login.php');
}

$page=explode('/', $_SERVER['REQUEST_URI']);

if (isset($_SESSION["blocked"])){ //Защита от неправильно введёного пароля 5 раз
            header("Location: {$_SERVER["HTTP_HOST"]}");
        }
?>
<div class="col-md-12 navbar-inverse">
    <div class="container">
        <a class="navbar-brand" href="/admin/index.php"><img src='/admin/src/logo.png' style="height:30px"></a>
    <div class="navbar-right">
    <form action="/admin/login.php" method="get" style="padding-top: 7px; margin-bottom: 0">
        <input type="submit" name="drop_session"  class="btn btn-sm btn-default" value="Выйти <?= $_SESSION['login'] ?>">
    </form>
        </div>
    </div>
</div>