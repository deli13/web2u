<html>
    <head>
        <meta charset="utf8">
        <title>Пользователи</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>tinymce.init({ 
                selector:'textarea',
                plugins: ['code autolink link image table']
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$index="Location: /admin/index.php";
if ($_SESSION["role"]!="admin"){
    header($index);
}
$table='t_user';
$conn=new dbquery($connect, $table);
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <div class="table_div">
                    <table class="table">
                        <thead>
                        <th>ID</th>
                        <th>Логин</th>
                        <th>Пароль</th>
                        <th>Права</th>
                        <th>Создан</th>
                        <th>Действие</th>
                        </thead>
                        <tbody>
                            <?php 
                            $query=$conn->select("");
                            foreach($query as $row){
                                echo "<tr>";
                                echo "<td>{$row["id_user"]}</td>";
                                echo "<td>{$row["login_user"]}</td>";
                                echo "<td>{$row["password_user"]}</td>";
                                echo "<td>{$row["role"]}</td>";
                                echo "<td>".dateNorm($row["created_user"])."</td>";
                                echo "<td><form action='/admin/controller/user_controller.php' name='delete' method='POST'>"
                                . "<input name='id' value='{$row['id_user']}' style='display:none'>"
                                . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                . "<form name='update'>"
                                . "<input name='id' value='{$row['id_user']}' style='display:none'>"
                                . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <p class="lead">Редактирование пользователей</p>
                <div class="form-group">
                    <form method="POST" action="/admin/controller/user_controller.php" name="sub">
                        <input name="id" id="id" value="" style="display:none">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Логин пользователя"><br/>
                        <input type="password" class="form-control" name="old-password" id="old-password" placeholder="Введите старый пароль" style="display: none"><br/> 
                        <input type="password" class="form-control" name="password" id="password" placeholder="Пароль"><br/>
                        <input type="password" class="form-control" name="repeat-password" id="repeat-password" placeholder="Повторите пароль"><br/>
                        <select name="role" id="role" class="form-control">
                            <option disabled selected>Выберите роль</option>
                            <option value="admin">Администратор</option>
                            <option value="moderator">Модератор</option>
                        </select><br/>
                        <input type="submit" class="form-control btn-primary" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
        <script>
        var pass=document.getElementById("password");
        var pass_rep=document.getElementById("repeat-password");
        var form=document.forms.sub;
        form.onsubmit=function(){
            if (pass.value!=pass_rep.value || pass.value.length<7){
                alert("Пароли не совпадают или меньше 8 символов");
                return false;
            } else {
                form.submit;
            }
        }
        
        $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/user_controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                        $('#id').val(req['id']);
                        $('#name').val(req['name']);
                        $('#role [value='+req['role']+']').attr("selected", "selected");
                        $('#old-password').css("display", "block");
                }
            })
            return false})
            
                    $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        </script>
    </body>
</html>