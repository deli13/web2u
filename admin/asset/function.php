<?php

function valid_input($znach){ //проверка введёных значений
    if (!is_numeric($znach)){
    
        $result = htmlentities(stripcslashes($znach));
    
    } else $result=$znach;
    return $result;
}

function session(){ //Старт сессии
    if (!isset($_SESSION)){
        session_start();
		ini_set('session.gc_maxlifetime', 1800);
		ini_set('session.cookie_lifetime', 1800);
    }
}

function translit($url){ //Создание транслитерации для ЧПУ
    $translit_table= array(
        "а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> "", "/"=> "_", '"'=>"", "'"=>"", "№"=>"num"
    );
    $url=  trim(mb_strtolower($url, 'UTF-8'));
            return strtr($url, $translit_table);
}

function translit1($url){ //Создание транслитерации для ЧПУ
    $translit_table= array(
        "а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
        " "=> "_", "."=> ".", "/"=> "_", '"'=>"", "'"=>"", "№"=>"num"
    );
    $url=  trim(mb_strtolower($url, 'UTF-8'));
            return strtr($url, $translit_table);
}


function crypto($pass){  //Хэш пароля
    $salt="239366";
    return sha1(md5($pass).$salt);
}

function dateNorm($date){  //Преобразование даты в человечий вид
    return date("d.m.Y", strtotime($date));
}

function imageUpload($file){
    $uploaddir='../../images/';
    $tmp=md5(microtime(true))."_".basename($file["name"]);
	$tmp=translit1($tmp);
    $uploadfile=$uploaddir.$tmp;
    if (move_uploaded_file($file['tmp_name'], $uploadfile)){
        return '/images/'.$tmp;
    } else {
        echo $file['error']."<br>";
        return false;
    }
}

function str200($text){
    $text_pr=  strip_tags($text); 
    if (strlen($text)>200){
        $text_pr=mb_substr($text_pr, 0, 200, 'UTF-8');
    }
    return $text_pr;
}

function strleng($text, $length){
    $text_pr=strip_tags($text);
    if (strlen($text_pr)>$length){
        $text_pr=mb_substr($text_pr,0,$length, 'UTF-8');
        }
    return $text_pr;
}

function postToUrl($url, $param){
	$result=file_get_contents($url, false, stream_context_create(array(
	'http'=>array(
	'method'=>'POST',
	'header'=>'Content-type: application/x-www-form-urlencoded',
	'content'=>http_build_query($param)
		)
	)));
	return $result;
}

function mailSend($mail, $mailclient, $subject, $msg){
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$to=$mailclient.", ".$mail;
	
	// Дополнительные заголовки
	$headers .= "To: klient <{$mailclient}>, w2you <{$mail}>" . "\r\n";
	$headers .= "From:  w2you <{$mail}>" . "\r\n";
	if(mail($to, $subject, $msg, $headers)){
		return true;
	} else {
		return false;
	}
	
}


?>

