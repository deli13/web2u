<html>
    <head>
        <meta charset="utf8">
        <title>Услуги</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$index="Location: /admin/index.php";
if ($_SESSION["role"]!="admin"){
    header($index);
}

$table='t_calc_osn';
$conn=new dbquery($connect, $table);
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
            	<div class='table_div'>
            		<table class='table'>
            		<thead>
            		<th>ID</th>
            		<th>Имя</th>
            		<th>Текст</th>
            		<th>Тип услуги</th>
            		<th>Цена</th>
            		<th>Действие</th>
            		</thead>
            		<tbody> 
            			<?php
            			$query=$conn->select("");
						foreach ($query as $row){
							echo "<tr>";
							echo "<td>{$row['id_osn']}</td>";
							echo "<td>{$row['name_osn']}</td>";
							echo "<td>{$row['text_osn']}</td>";
							if ($row['type_module']==1){
								echo "<td>Основные услуги</td>";
							} else {
								echo "<td>Дополнительные услуги</td>";
							}
							echo "<td>{$row['price_osn']}</td>";
							echo "<td><form name='delete' action='/admin/controller/CalcOsn_controller.php' method='POST'>"
                                        . "<input name='id' value='{$row['id_osn']}' style='display:none'>"
                                        . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                        . "<form name='update'>"
                                        . "<input name='id' value='{$row['id_osn']}' style='display:none'>"
                                        . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                           echo "</tr>";
						}
            			?>
            		</tbody>
            		</table>
            	</div>
            	<p class="lead">Редактирование услуг</p>
            	<form action="/admin/controller/CalcOsn_controller.php" method="post">
            		<input name="id" id="id" style="display:none">
            		<input name="name" id="name" class="form-control" placeholder="Наименование услуги"><br/>
            		<textarea name="text" id="text" class="form-control" rows="15"></textarea><br/>
            		<select name="type" id="type" class="form-control">
            		<option selected disabled>Выберите тип услуги</option>
            		<option value="1">Основные услуги</option>
            		<option value="0">Дополнительные услуги</option>
            		</select><br/>
            		<div class='input-group'>
            		<span class="input-group-addon">Цена</span>
            		<input name="price" id="price"  type="number" class="form-control">
            		</div><br/>
            		<a class="btn btn-success" id="content" style="display: none; margin-right: 5px" href="#" role="button">Модули услуг</a>
            		<br/>
            		<input type="submit" class="form-control btn btn btn-primary">
            	</form>
            </div>
       	</div>
       	<script>
       	    $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/CalcOsn_controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                            $("#id").val(req["id"]);
                            $("#name").val(req["name"]);
                            $("#text").text(req["text"]);
                           	$("#price").val(req["price"]);
                            tinyMCE.activeEditor.setContent(req['text']);
                            $('#type [value="'+req["type"]+'"]').attr("selected", "selected");
                            if(req["type"]==1){
                            	$("#content").css("display","block");
                            	$("#content").attr("href", "/admin/calculatorDop.php?osn="+req['id']+"");
                            }
                }
            })
            return false})
            
            $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
       	</script>
   	</body>
</html>