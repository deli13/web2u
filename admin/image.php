<html>
    <head>
        <meta charset="utf8">
        <title>Изображения</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
            <script>tinymce.init({ 
                selector:'textarea',
                plugins: ['code autolink link image table']
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();

$table='t_img';
$table_publication="t_publication";
$conn=new dbquery($connect, $table);
$conn_pub=new dbquery($connect, $table_publication);
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <br/>
               
                <form method="POST" enctype="multipart/form-data" action="/admin/controller/image-controller.php">
                   
                    <select class="form-control" id="id_pub" required name="id_pub">
                        <option selected disabled>Выберите публикацию</option>
                        <?php
                        $query=$conn_pub->selectColumn('id_publication, name_publication');
                        foreach ($query as $row) {
                            if ($_GET['public']==$row['id_publication']){
                                echo "<option selected value='".$row['id_publication']."'>".$row['name_publication']."</option>"; 
                                continue;
                            }
                            echo "<option value='".$row['id_publication']."'>".$row['name_publication']."</option>";
                            }
                        ?>
                        </select>
                    <script>
                    
                    id_pub=document.getElementById("id_pub");  //Скрипт для обновления в зависимости от выбора публикации
                    id_pub.addEventListener('change', function(){
                        var form=document.createElement("form");
                        form.setAttribute('method','get');
                        form.setAttribute('action','/admin/image.php');
                        this.setAttribute('name', 'public');
                        form.appendChild(this);
                        form.submit();
                    })
                    </script>
                        <?php
                        if (isset($_GET["public"])){
                            $public=$_GET["public"];
                            $row=$conn_pub->selectJoin($table,"id_img", "t_publication.id_publication=".$public);
                            $row=$row[0];
                        }
                        ?>
                    <input name="id" style="display: none" value="<?php echo @$row["id_img"]?>">
                    <br/>
                    
                        <div class="row">
                        <div class="col-md-6">
                        <label class="btn btn-default btn-file">
                            Логотип<input type="file" accept="image/*"  name="logo">
                        </label><br/></div>
                        <div class="col-md-6">
                            <img height="100" src="<?php echo @$row["img_logo"] ?>" id="logo">
                        </div>
                        </div>
                    
                        <div class="row">
                        <div class="col-md-6">    
                        <label class="btn btn-default btn-file">
                            Изображение для ПК<input type="file" accept="image/*"  name="desktop">
                        </label><br/>
                        </div>
                        <div class="col-md-6">
                            <img height="100" src="<?php  echo @$row["img_desktop"] ?>" id="img-desktop">
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">    
                        <label class="btn btn-default btn-file">
                            Изображение для ПК 2<input type="file" accept="image/*"  name="desktop2">
                        </label><br/>
                        </div>
                        <div class="col-md-6">
                            <img height="100" src="<?php  echo @$row["img_desktop_1"] ?>" id="img-desktop2">
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">    
                        <label class="btn btn-default btn-file">
                            Изображение для ПК 3<input type="file" accept="image/*"  name="desktop3">
                        </label><br/>
                        </div>
                        <div class="col-md-6">
                            <img height="100" src="<?php  echo @$row["img_desktop_2"] ?>" id="img-desktop3">
                        </div>
                        </div>
                        
						<div class="row">
                        <div class="col-md-6">    
                        <label class="btn btn-default btn-file">
                            Изображение для ПК 4<input type="file" accept="image/*"  name="desktop4">
                        </label><br/>
                        </div>
                        <div class="col-md-6">
                            <img height="100" src="<?php  echo @$row["img_desktop_3"] ?>" id="img-desktop4">
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">    
                        <label class="btn btn-default btn-file">
                            Изображение для ПК 5<input type="file" accept="image/*"  name="desktop5">
                        </label><br/>
                        </div>
                        <div class="col-md-6">
                            <img height="100" src="<?php  echo @$row["img_desktop_4"] ?>" id="img-desktop5">
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                        <label class="btn btn-default btn-file">
                            Изображение для планшета вертикальный<input type="file" accept="image/*"  name="tablev">
                        </label><br/></div>
                        <div class="col-md-6">
                            <img height="100" src="<?php echo @$row["img_tablet_vert"] ?>" id="img-tabletv">
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                        <label class="btn btn-default btn-file">
                            Изображение для планшета горизонтальный<input type="file" accept="image/*"  name="tableh">
                        </label><br/>
                        </div>
                        <div class="col-md-6">
                            <img height="100" src="<?php echo @$row["img_tablet_horiz"] ?>" id="img-tableth">
                        </div>
                        </div>
                            
                        <div class="row">
                        <div class="col-md-6">
                        <label class="btn btn-default btn-file">
                            Изображение для телефона вертикальный<input type="file" accept="image/*"  name="phonev">
                        </label><br/></div>
                        <div class="col-md-6">
                            <img height="100" src="<?php echo @$row["img_phone_vert"] ?>" id="img-phonev">
                        </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-md-6">
                        <label class="btn btn-default btn-file">
                            Изображение для телефона горизонтальный<input type="file" accept="image/*"  name="phoneh">
                        </label><br/></div>
                        <div class="col-md-6">
                            <img height="100" src="<?php echo @$row["img_phone_horiz"] ?>" id="img-phoneh">
                        </div>
                        </div><br/>
                        <input type="submit" class="form-control btn-primary" value="Сохранить">
                    </form>
                
            </div>
        </div>
    </body>
</html>
