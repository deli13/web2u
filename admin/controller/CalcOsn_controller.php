<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");
$header="Location: ".$_SERVER["HTTP_REFERER"];
$table='t_calc_osn';
$table_dop='t_calc_dop';
$conn=new dbquery($connect, $table);
$conn_dop=new dbquery($connect, $table_dop);

if ((isset($_POST['name'])) && ($_POST['id']=="")){
	$name=$_POST['name'];
	$text=$_POST['text'];
	$price=$_POST['price'];
	$type=$_POST['type'];
	$field=array('name_osn', 'text_osn', 'price_osn', "type_module");
	$value=array($name, $text, $price, $type);
	$conn->insert($field, $value);
	header($header);
}

if (isset($_POST['delete'])){
	$id=$_POST['id'];
	$conn_dop->delete("id_osn={$id}");
	$conn->delete("id_osn={$id}");
	header($header);
}

if (isset($_POST['update'])){
	$id=$_POST['id'];
	$query=$conn->select("id_osn={$id}");
	$row=$query[0];
	$id=$row['id_osn'];
	$name=$row['name_osn'];
	$text=$row['text_osn'];
	$price=$row['price_osn'];
	$type=$row['type_module'];
	$jsonarr=array('id'=>$id, 'name'=>$name, 'text'=>$text, "price"=>$price, "type"=>$type);
	echo json_encode($jsonarr);
}

if ((isset($_POST['name'])) && ($_POST['id']!="")){
	$id=$_POST['id'];
	$name=$_POST['name'];
	$text=$_POST['text'];
	$price=$_POST['price'];
	$type=$_POST['type'];
	$field=array('name_osn', 'text_osn', 'price_osn', 'type_module');
	$value=array($name, $text, $price,$type);
	$conn->update($field, $value, "id_osn={$id}");
	header($header);
}


 
?>