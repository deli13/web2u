<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$table_name='t_brand';
$conn=new dbquery($connect, $table_name);
$header="Location: ".$_SERVER["HTTP_REFERER"];
define("noimage", "/images/noimage.png");

if (!is_dir('../../images')){
    mkdir('../../images');
}

if ((isset($_POST['name'])) && ($_POST['id']=='')){
    $file=$_FILES['file'];
    $name=$_POST['name'];
    $description=$_POST['description'];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $image=  imageUpload($file); //Загрузка Изображения
    if ($image==false) $image=noimage;
    $field=array('name_brand','description_brand', 'img_brand',  "title_brand", "descr_brand", "keywords_brand");
    $values=array($name, $description, $image, $title, $descr_seo, $keywords);
    $query=$conn->insert($field, $values);
    header($header);
}

if (isset($_POST['delete'])){
    $id=$_POST['id'];
    $conn_publication=new dbquery($connect, 't_publication');
    $result_publication=$conn_publication->select("id_brand=".$id);
    if (count($result_publication)>=1){
        echo 'Удаление не возможно, запись используется<br>'
        .'<a href="/admin/brand.php">';
        die();
    }
    $res=$conn->select("id_brand=".$id);
    if ($res[0]['img_brand']!=noimage) unlink('../..'.$res[0]['img_brand']);
    $conn->delete("id_brand=".$id);
    header($header);
}

if (isset($_POST['update'])){
    $id=$_POST['id'];
    $res=$conn->select('id_brand='.$id);
    $json=array('id'=>$res[0]['id_brand'], 'name'=>$res[0]['name_brand'], 'descr'=>$res[0]['description_brand'], 'img'=>$res[0]['img_brand'], 'title'=>$res[0]['title_brand'], 'descr_seo'=>$res[0]["descr_brand"], 'keywords'=>$res[0]['keywords_brand']);
    echo json_encode($json);
}

if ((isset($_POST['name'])) && $_POST['id']!=""){
    $id=$_POST['id'];
    $name=$_POST['name'];
    $description=$_POST['description'];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    if ($_FILES["file"]["name"]!=""){
        $res=$conn->select("id_brand=".$id);
        if ($res[0]['image_brand']!=noimage) unlink('../..'.$res[0]['img_brand']);
        $img=imageUpload($_FILES["file"]);
        $conn->update('img_brand', $img, 'id_brand='.$id);
    }
    $field=array('name_brand', 'description_brand', 'updated_brand',  "title_brand", "descr_brand", "keywords_brand");
    $value=array($name, $description, date("Y-m-d H:i:s"),  $title, $descr_seo, $keywords);
    $conn->update($field, $value, "id_brand=".$id);
    header($header);
}
