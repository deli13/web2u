<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");

$table='t_publication';
$table_brand='t_brand';
$table_img='t_img';
$table_category='t_category';
$table_hashtag='t_hashtag_publication';
$table_content='t_content';
$conn=new dbquery($connect, $table);
$conn_img=new dbquery($connect, $table_img);
$header="Location: ".$_SERVER["HTTP_REFERER"];

if ((isset($_POST['name'])) && ($_POST['id']=="")){
    $name=$_POST['name'];
    $description=$_POST['descr'];
    $category=$_POST['id_category'];
    $brand=(isset($_POST['id_brand']))?$_POST['id_brand']:'';
    $price=$_POST['price'];
    $otziv=$_FILES['file'];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $dop_usl= json_encode($_POST['add_usl']);
    $viv=(!is_numeric($_POST['viv']))?0:$_POST['viv'];
    $otziv=  imageUpload($otziv);
    $cpu=  translit($name);
    if ($otziv==false) $otziv=noimage;
    $field=array('name_publication','description','id_category','id_brand','price', 'otziv_publication', 'cpu', 'title_publication', 'descr_publication', 'keywords_publication', 'viv_publication', 'dop_uslugi_publication');
    $values=array($name, $description, $category, $brand, $price,$otziv, $cpu, $title, $descr_seo, $keywords, $viv, $dop_usl);
    $newid=$conn->insert($field, $values);
    header($header."?public=".$newid);
}

if(isset($_POST['delete'])){
    $id=$_POST['id'];
    $conn_content=new dbquery($connect, $table_content);
    $conn_hashtag=new dbquery($connect, $table_hashtag);
    $return=$conn->select("id_publication=".$id);
    $conn_content->delete("id_publication=".$id);
    $conn_hashtag->delete("id_publication=".$id);
    $conn->delete("id_publication=".$id);
	$url='http://'.$_SERVER["HTTP_HOST"].'/admin/controller/image-controller.php';
	$param_img=array('delete'=>1, 'id'=>$return[0]['id_img']);
	echo $url;
	print_r($param_img);
    if ($return[0]['id_img']!='') echo postToUrl($url,$param_img);
    if ($return[0]['otziv_publication']!=noimage) unlink('../../'.$return[0]['otziv_publication']);
	
    header($header);
}

if (isset($_POST['update'])){
    $id=$_POST['id'];
    $query=$conn->select("id_publication=".$id);
    $name=$query[0]['name_publication'];
    $descr=$query[0]['description'];
    $id_cat=$query[0]['id_category'];
    $id_brand=$query[0]['id_brand'];
    $otziv=$query[0]['otziv_publication'];
    $id_img=$query[0]['id_img'];
    $price=$query[0]['price'];
    $title=$query[0]['title_publication'];
    $descr_seo=$query[0]['descr_publication'];
    $keywords=$query[0]['keywords_publication'];
    $viv=$query[0]['viv_publication'];
    $dop_usl=json_decode($query[0]['dop_uslugi_publication'], 1);
    $count_dop=count($dop_usl);
    $string_usl="";
    if ($count_dop>0){
	    $where="(";
	    for ($i=0; $i<$count_dop; $i++){
	        if ($i<$count_dop-1){
	            $where.="id_publication={$dop_usl[$i]} or ";
	        } else {
	            $where.="id_publication={$dop_usl[$i]})";
	        }
	    }
	    $query_usl=$conn->select($where);
	    
	    foreach($query_usl as $val){
	        $string_usl.=$val["name_publication"].", ";
	    }
	}
    $json=array('id'=>$id, 'name'=>$name, 'descr'=>$descr,'viv'=>$viv, 'category'=>$id_cat, 'brand'=>$id_brand, 'otziv'=>$otziv, 'img'=>$id_img, 'price'=>$price, 'title'=>$title, 'descr_seo'=>$descr_seo, 'keywords'=>$keywords, "usl"=>$string_usl);
    echo json_encode($json);
    //echo $dop_usl;
}

if ((isset($_POST['name'])) && ($_POST['id']!='')){
    $id=$_POST['id'];
    $res=$conn->select("id_publication=".$id);
    $name=$_POST['name'];
    $description=$_POST['descr'];
    $category=$_POST['id_category'];
    $brand=(isset($_POST['id_brand']))?$_POST['id_brand']:'';
    $price=$_POST['price'];
    $cpu=  translit($name);
    $dop_usl=($_POST['add_usl']!="")?json_encode($_POST['add_usl']):$res[0]["dop_uslugi_publication"];
    $title=$_POST['title'];
    $viv=(!is_numeric($_POST['viv']))?0:$_POST['viv'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    if ($_FILES['file']['name']!=""){
        if ($res[0]['otziv_publication']!=noimage) unlink("../..".$res[0]['otziv_publication']);
        $files=  imageUpload($_FILES['file']);
        $conn->update('otziv_publication', $files, 'id_publication='.$id);
    }
    $field=array('name_publication','viv_publication', 'description', 'id_category', 'id_brand', 'price', 'cpu', "updated_publication", 'title_publication', 'descr_publication', 'keywords_publication',"dop_uslugi_publication");
    $value=array($name,$viv, $description, $category, $brand, $price, $cpu, date("Y-m-d H:i:s"), $title, $descr_seo, $keywords,$dop_usl);
    $conn->update($field, $value, 'id_publication='.$id);
    $header=explode("?",$_SERVER["HTTP_REFERER"]);
    header("Location: {$header[0]}"."?id={$id}");
}

?>
