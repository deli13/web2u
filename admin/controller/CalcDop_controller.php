<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';

$table="t_calc_dop";
$table_osn="t_calc_osn";
$conn= new dbquery($connect, $table);
$header="Location: ".$_SERVER["HTTP_REFERER"];

if ((isset($_POST['name'])) && ($_POST['id']=="")){
	$name=$_POST['name'];
	$text=$_POST['text'];
	$osn_id=$_POST['osn'];
	$price=$_POST['price'];
	$field=array('name_dop', 'text_dop', 'price_dop', 'id_osn');
	$value=array($name, $text, $price, $osn_id);
	$conn->insert($field, $value);
	header($header);
}

if (isset($_POST['delete'])){
	$id=$_POST['id'];
	$conn->delete("id_dop={$id}");
	header($header);
}

if (isset($_POST['update'])){
	$id=$_POST['id'];
	$query=$conn->select("id_dop={$id}");
	$row=$query[0];
	$id_osn=$row['id_osn'];
	$name=$row['name_dop'];
	$text=$row['text_dop'];
	$price=$row['price_dop'];
	$jsonarr=array('id'=>$id, "osn"=>$id_osn, "name"=>$name, "text"=>$text, "price"=>$price);
	echo json_encode($jsonarr);
}

if ((isset($_POST["name"])) && ($_POST["id"]!="")){
	$id=$_POST["id"];
	$name=$_POST["name"];
	$text=$_POST["text"];
	$osn=$_POST["osn"];
	$price=$_POST["price"];
	$field=array("name_dop", "text_dop", "id_osn", "price_dop");
	$value=array($name, $text, $osn, $price);
	$conn->update($field, $value, "id_dop={$id}");
	header($header);
}
?>
