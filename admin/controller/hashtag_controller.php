<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
$header="Location: ".$_SERVER["HTTP_REFERER"];

$table_hashtag="t_hashtag";
$table_publication="t_publication";
$table_sviaz="t_hashtag_publication";
$conn=new dbquery($connect, $table_hashtag);
$conn_sviaz=new dbquery($connect, $table_sviaz);

if (isset($_POST["new"])){ //Добавление хэтегов
    $hashtag=$_POST["hashtag"];
    $conn->insert("hashtag", $hashtag);
    $query=$conn->select("");
    echo json_encode($query);
}

if (isset($_POST["select"])){ //Вывод в JSON всех доступных хештегов AJAX
    $query=$conn->select("");
    echo json_encode($query);
}

if (isset($_POST["insert_hashtag"])){//Добавление хэштегов
    $id=$_POST["id"];
    $conn_sviaz->delete("id_publication=".$id);//Удаляем все старые хэштеги
    $hashtag_arr=$_POST["insert_hashtag"];
    $field=array("id_hashtag", "id_publication");
    foreach($hashtag_arr as $hashtag){
        $values=array($hashtag, $id);
        $conn_sviaz->insert($field, $values);
    }
    header($header);
}

if (isset($_POST["select_hashtag"])){ //Вывод хэштегов для отдельной публикации AJAX
    $id_pub=$_POST["id_pub"];
    //echo $id_pub;
    $field=array($table_hashtag, $table_publication);
    $value=array("id_hashtag", "id_publication");
    $select=$conn_sviaz->selectJoin($field, $value, $table_publication.".id_publication=".$id_pub);
    $hash=array();
    foreach ($select as $row){
        $hash[]=$row["hashtag"];
    }
    echo json_encode($hash);
}




?>