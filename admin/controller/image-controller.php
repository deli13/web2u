<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");
$table="t_img";
$table_publication="t_publication";
$conn=new dbquery($connect, $table);
$conn_public=new dbquery($connect, $table_publication);
$header="Location: ".$_SERVER["HTTP_REFERER"];


if (isset($_POST['delete'])){
	$id_img=$_POST['id'];
	$result=$conn->select('id_img='.$id_img);
	$row=$result[0];
	if($row['img_desktop']!=noimage) unlink("../..".$row['img_desktop']);
	if($row['img_tablet_vert']!=noimage) unlink("../..".$row['img_tablet_vert']);
	if($row['img_tablet_horiz']!=noimage) unlink("../..".$row['img_tablet_horiz']);
	if($row['img_phone_vert']!=noimage) unlink("../..".$row['img_phone_vert']);
	if($row['img_phone_horiz']!=noimage) unlink("../..".$row['img_phone_horiz']);
	if($row['img_logo']!=noimage) unlink("../..".$row['img_logo']);
	if($row['img_desktop_1']!=noimage) unlink("../..".$row['img_desktop_1']);
	if($row['img_desktop_2']!=noimage) unlink("../..".$row['img_desktop_2']);
	if($row['img_desktop_3']!=noimage) unlink("../..".$row['img_desktop_3']);
	if($row['img_desktop_4']!=noimage) unlink("../..".$row['img_desktop_4']);
	$conn->delete('id_img='.$id_img);
	echo 'delete all';
}


if ((!isset($_POST["id_pub"])) || ($_POST["id_pub"]=="")){
    echo "Не выбрана публикация для добавления<br/>"
    . "<a href='{$_SERVER["HTTP_REFERER"]}'>Вернуться для редактирования";
    die();
}

if ((isset($_POST["id_pub"])) && ($_POST["id"]=="")){  //Закачка новых изображений
    $imaged4=$imaged3=$imaged2=$imaged1=$imagelogo=$imaged=$imagetabv=$imagetabh=$imagephv=$imagephh=noimage;
    if ($_FILES["logo"]["name"]!='') $imagelogo=imageUpload($_FILES["logo"]);
    if ($_FILES["desktop"]["name"]!='') $imaged=imageUpload($_FILES["desktop"]);
	if ($_FILES["desktop2"]["name"]!='') $imaged1=imageUpload($_FILES["desktop2"]);
	if ($_FILES["desktop3"]["name"]!='') $imaged2=imageUpload($_FILES["desktop3"]);
	if ($_FILES["desktop4"]["name"]!='') $imaged3=imageUpload($_FILES["desktop4"]);
	if ($_FILES["desktop5"]["name"]!='') $imaged4=imageUpload($_FILES["desktop5"]);
    if ($_FILES["tablev"]["name"]!='') $imagetabv=imageUpload($_FILES["tablev"]);
    if ($_FILES["tableh"]["name"]!='') $imagetabh=imageUpload($_FILES["tableh"]);
    if ($_FILES["phonev"]["name"]!='') $imagephv=imageUpload($_FILES['phonev']);
    if ($_FILES["phoneh"]["name"]!='') $imagephh=imageUpload($_FILES["phoneh"]);
    $field=array("img_desktop", "img_tablet_vert", "img_tablet_horiz", "img_phone_vert", "img_phone_horiz", "img_logo", "img_desktop_1", "img_desktop_2", "img_desktop_3", "img_desktop_4");
    $values=array($imaged, $imagetabv, $imagetabh, $imagephv, $imagephh, $imagelogo, $imaged1, $imaged2, $imaged3, $imaged4);
    $id_img=$conn->insert($field, $values);
    $id_pub=$_POST["id_pub"];
    print_r($id_img);
    $conn_public->update("id_img", $id_img, "id_publication=".$id_pub);
    header($header);
} 

if ((isset($_POST["id_pub"])) && ($_POST["id"]!="")){ //Обновление существующих
    $id_img=$_POST["id"];
    $result=$conn->select("id_img=".$id_img);
    $imagelogo=$result[0]['img_logo'];
    $imaged=$result[0]['img_desktop'];
    $imagetabv=$result[0]["img_tablet_vert"];
    $imagetabh=$result[0]["img_tablet_horiz"];
    $imagephv=$result[0]["img_phone_vert"];
    $imagephh=$result[0]["img_phone_horiz"];
    $imaged1=$result[0]['img_desktop_1'];
	$imaged2=$result[0]['img_desktop_2'];
	$imaged3=$result[0]['img_desktop_3'];
	$imaged4=$result[0]['img_desktop_4'];
    if ($_FILES["logo"]["name"]!="") {
       if ($imagelogo!=noimage) unlink("../..".$imagelogo);
       $imagelogo=imageUpload($_FILES["logo"]);
    }
    if ($_FILES["desktop"]["name"]!="") {
       if ($imaged!=noimage) unlink("../..".$imaged);
       $imaged=imageUpload($_FILES["desktop"]);
    }
	if ($_FILES["desktop2"]["name"]!="") {
       if ($imaged1!=noimage) unlink("../..".$imaged1);
       $imaged1=imageUpload($_FILES["desktop2"]);
    }
	if ($_FILES["desktop3"]["name"]!="") {
       if ($imaged1!=noimage) unlink("../..".$imaged2);
       $imaged2=imageUpload($_FILES["desktop3"]);
    }
	if ($_FILES["desktop4"]["name"]!="") {
       if ($imaged1!=noimage) unlink("../..".$imaged3);
       $imaged3=imageUpload($_FILES["desktop4"]);
    }
	if ($_FILES["desktop5"]["name"]!="") {
       if ($imaged1!=noimage) unlink("../..".$imaged4);
       $imaged4=imageUpload($_FILES["desktop5"]);
    }
    if ($_FILES["tablev"]["name"]!=""){
        if ($imagetabv!=noimage) unlink("../..".$imagetabv);
        $imagetabv=  imageUpload($_FILES["tablev"]);
    }
    if ($_FILES['tableh']['name']!=""){
        if ($imagetabh!=noimage) unlink("../..".$imagetabh);
        $imagetabh=  imageUpload($_FILES['tableh']);
    }
    if ($_FILES["phonev"]["name"]!=""){
        if ($imagephv!=noimage) unlink("../..".$imagephv);
        $imagephv=  imageUpload($_FILES["phonev"]);
    }
    if ($_FILES["phoneh"]["name"]!=""){
        if ($imagephh!=noimage) unlink("../..".$imagephh);
        $imagephh=  imageUpload($_FILES["phoneh"]);
    }
    $field=array("img_desktop", "img_tablet_vert", "img_tablet_horiz", "img_phone_vert", "img_phone_horiz", "img_logo", "img_desktop_1", "img_desktop_2", "img_desktop_3", "img_desktop_4");
    $value=array($imaged, $imagetabv, $imagetabh, $imagephv, $imagephh, $imagelogo, $imaged1, $imaged2, $imaged3, $imaged4);
    $where="id_img=".$id_img;
    $conn->update($field, $value, $where);
    header($header);
}


?>