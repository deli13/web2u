<?php
require_once '../asset/function.php';
require_once '../model/connect.php';
require_once '../model/dbquery.php';
define("noimage", "/images/noimage.png");
$header="Location: ".$_SERVER["HTTP_REFERER"];
$table='t_news';
$table_content='t_content';

$conn=new dbquery($connect, $table);
$conn_cont=new dbquery($connect, $table_content);

if ((isset($_POST['name'])) && ($_POST['id']=='')){ //Добавление новости
    $name=$_POST['name'];
    $text=$_POST['text'];
    $cpu=translit($name);
    $img=noimage;
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    if ($_FILES['file']['name']!="") $img= imageUpload($_FILES['file']); //Загрузка изображения
    $field=array('name_content', 'text_content', 'img_content');
    $value=array($name, $text, $img);
    $id_cont=$conn_cont->insert($field, $value); //добавление текста новости 
    $field_news=array("name_news", "id_content", "cpu", "title_news", "descr_news", "keywords_news");
    $value_news=array($name, $id_cont, $cpu, $title, $descr_seo, $keywords);
    $conn->insert($field_news, $value_news);
    header($header);
}

if (isset($_POST['delete'])){ //Удаление новости и ссвязанного контента
    $id=$_POST['id'];
    $query=$conn->selectJoin($table_content, 'id_content', "id_news=".$id);
    //$query_cont=$conn_cont->select("id_content=".$query[0]["id_content"]);
    $query=$query[0];
    $img=$query['img_content'];
    $id_cont=$query['id_content'];
    if ($img!=noimage){
        unlink("../..".$img);
    }
    $conn->delete("id_news=".$id);
    $conn_cont->delete("id_content=".$id_cont);
    header($header);
}

if (isset($_POST['update'])){ //Подготовка JSON для Обновления
    $id=$_POST['id'];
    $query=$conn->selectJoin($table_content, 'id_content', 'id_news='.$id);
    $result=$query[0];
    $name=$result['name_news'];
    $text=$result['text_content'];
    $img=$result['img_content'];
    $title=$result['title_news'];
    $descr_seo=$result['descr_news'];
    $keywords=$result['keywords_news'];
    $json=array("id"=>$id, "name"=>$name, "text"=>$text, "img"=>$img,  'title'=>$title, 'descr_seo'=>$descr_seo, 'keywords'=>$keywords);
    echo json_encode($json);
}

if ((isset($_POST["name"])) && ($_POST["id"]!="")){ //Обновление новостей
    $id=$_POST["id"];
    $name=$_POST["name"];
    $text=$_POST["text"];
    $title=$_POST['title'];
    $descr_seo=$_POST['descr_seo'];
    $keywords=$_POST['keywords'];
    $query=$conn->selectJoin($table_content, "id_content", "id_news=".$id);
    $result=$query[0];
    $id_content=$result["id_content"];
    $img=$result["img_content"];
    if ($_FILES["file"]["name"]!=""){
        unlink("../..".$img);
        $img=imageUpload($_FILES['file']);
        //$conn_cont->update("img_content", $img, "id_content=".$id_content);
    }
    $field=array("name_content", "text_content", "updated_content", "img_content");
    $value=array($name, $text, date("Y-m-d H:i:s"), $img);
    $conn_cont->update($field, $value, "id_content=".$id_content);
    $field_news=array("name_news", "updated_news", "cpu",  "title_news", "descr_news", "keywords_news");
    $value_news=array($name, date("Y-m-d H:i:s"), translit($name),  $title, $descr_seo, $keywords);
    $conn->update($field_news, $value_news, "id_news=".$id);
    $header=explode("?",$_SERVER["HTTP_REFERER"]);
    header("Location: {$header[0]}"."?id={$id}");
}
?>