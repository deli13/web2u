<?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
if ((!isset($_SESSION['id'])) && (!isset($_SESSION['login']))){
    session_destroy();
    header('Location: /admin/login.php');
}

$table_category='t_category';

$conn_left=new dbquery($connect, $table_category);
$query_left=$conn_left->select("");
?>
<div class="col-md-2">
   
        <ul class="nav nav-pills nav-stacked">
            <?php if($_SESSION["role"]=="admin"){?>
            <li class=""><a href="files.php">Файловый менеджер</a></li>
            <li class=""><a href="user.php">Пользователи</a></li>
            <li class=""><a href="routing.php">Маршрутизация</a></li>
            <li class=""><a href="template.php">Шаблоны</a></li>
            
            <li class=""><a href='calculatorOsn.php'>Услуги</a></li>
            <li class=""><a href='calculatorDop.php'>Модули для услуг</a></li> 
            <?php }?>
            <li class=""><a href="category.php">Категории</a></li>
            <li class=""><a href="brand.php">Раздел CMS</a></li>
            <li class="dropdown-header">
            	<div class="btn-group">
	            	<a href="publication.php" role="button" class="btn btn-default">Публикация</a>
	            	<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" class="dropdown-toggle"><span class="caret"></span></button>
			            <ul class="dropdown-menu" >
			            	<?php foreach($query_left as $val){
			            		echo "<li><a href=publication.php?search-cat={$val["id_category"]}&search=Поиск>{$val["name_category"]}</a></li>";
			            	}
			            	?>
			            </ul>
	            </div> 
            </li>
            <li class=""><a href="image.php">Изображения</a></li>
            <li class=""><a href="content.php">Контент</a></li>
            <li class=""><a href="news.php">Новости</a></li>
        </ul>
    <script>
        var title=$("title").text();
        if (title!=""){
            var selected=$("li a:contains('"+title+"')");
            selected.parent().addClass('active');
        }
    </script>
       
    </div>

