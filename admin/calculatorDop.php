<html>
    <head>
        <meta charset="utf8">
        <title>Модули для услуг</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
$index="Location: /admin/index.php";
if ($_SESSION["role"]!="admin"){
    header($index);
}

$table='t_calc_dop';
$table_osn="t_calc_osn";
$conn=new dbquery($connect, $table);
$conn_osn=new dbquery($connect, $table_osn);
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
            	<div class='table_div'>
            		<table class='table'>
            		<thead>
            		<th>ID</th>
            		<th>Имя</th>
            		<th>Имя основной категории</th>
            		<th>Текст</th>
            		<th>Цена</th>
            		<th>Действие</th>
            		</thead>
            		<tbody> 
            			<?php
            			if (isset($_GET["osn"])){
            				$id=$connect->quote($_GET['osn']);
            				$query=$conn->selectJoin($table_osn, "id_osn", $table.".id_osn={$id}");
            			} else $query=$conn->selectJoin($table_osn, "id_osn");
            			
						foreach ($query as $row){
							echo "<tr>";
							echo "<td>{$row['id_dop']}</td>";
							echo "<td>{$row['name_dop']}</td>";
							echo "<td>{$row['name_osn']}</td>";
							echo "<td>{$row['text_dop']}</td>";
							echo "<td>{$row['price_dop']}</td>";
							echo "<td><form name='delete' action='/admin/controller/CalcDop_controller.php' method='POST'>"
                                        . "<input name='id' value='{$row['id_dop']}' style='display:none'>"
                                        . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                        . "<form name='update'>"
                                        . "<input name='id' value='{$row['id_dop']}' style='display:none'>"
                                        . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form></td>";
                            echo "</tr>";
						}
            			?>
            		</tbody>
            		</table>
            	</div>
            	<p class="lead">Редактирование дополнительных услуг</p>
            	<form action="/admin/controller/CalcDop_controller.php" method="post">
            		<input name="id" id="id" style="display:none">
            		<input name="name" id="name" class="form-control" placeholder="Наименование услуги"><br/>
            		<textarea name="text" id="text" class="form-control" rows="15"></textarea><br/>
            		<select name="osn" id="osn" class="form-control">
            		<option disabled selected>Выберите услугу</option>
            		<?php 
            		$query_osn=$conn_osn->select("");
					foreach($query_osn as $rows){
						if ($rows["type_module"]!=1) continue;
						if ((isset($_GET['osn'])) &&  ($rows['id_osn']==$_GET['osn'])){
							echo "<option selected value='{$rows['id_osn']}'>{$rows['name_osn']}</option>";
							continue;
						}
							echo "<option value='{$rows['id_osn']}'>{$rows['name_osn']}</option>";
					}
            		?>
            		</select><br/>
            		<div class='input-group'>
            		<span class="input-group-addon">Цена</span>
            		<input name="price" id="price"  type="number" class="form-control">
            		</div>
            		<br/>
            		<input type="submit" class="form-control btn btn btn-primary">
            	</form>
            </div>
       	</div>
       	<script>
       	    $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/CalcDop_controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                            $("#id").val(req["id"]);
                            $("#name").val(req["name"]);
                            $("#text").text(req["text"]);
                           	$("#price").val(req["price"]);
                           	$("#osn [value='"+req["osn"]+"']").attr("selected", "selected");
                            tinyMCE.activeEditor.setContent(req['text']);
                }
            })
            return false})
            
            $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
       	</script>
   	</body>
</html>