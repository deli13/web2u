<html>
    <head>
        <meta charset="utf8">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script   src="http://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
        <script async src="js/bootstrap.min.js"></script>
    </head>  

<?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
        session();
        
        if (isset($_SESSION["blocked"])){ //Защита от неправильно введёного пароля 5 раз
            header("Location: http://{$_SERVER["HTTP_HOST"]}");
        }
?>
    <body>
        <div class="container">
        <h2>Войти в систему управления</h2>
        <form method='post' action='/admin/login.php'>
            <input type='text' name="login" placeholder="login" class="form-control"><br />
            <input type='password' name="password" placeholder='password' class="form-control"><br />
            <input type='submit' class="btn" value="Войти">
        </form>

    <?php
    if (!isset($_SESSION["count"])){
        $_SESSION["count"]=1;
    }
    
    if ((isset($_POST['login'])) && (isset($_POST['password']))){
        $login=  valid_input($_POST['login']);
        $password= crypto(valid_input($_POST['password']));
        $query= new dbquery($connect, 't_user');
        $where="login_user='{$login}' and password_user='{$password}'";
        $result=$query->select($where);
        if (count($result)!=1){
            echo "<div class='alert alert-danger'>Неправильный логин или пароль</div>";
            $_SESSION["count"]++;
            if ($_SESSION["count"]==6){ //Защита от неправильно введённого пароля
                $_SESSION["blocked"]="";
            }
        } else {
            $_SESSION['id']=$result[0]['id_user'];
            $_SESSION['login']=$result[0]['login_user'];
            $_SESSION['role']=$result[0]['role'];
            header("Location: /admin/index.php");
            //echo "id=".$_SESSION['id']." login=".$_SESSION['login'];
        }
    }
    
    if (isset($_GET['drop_session'])){
        session_destroy();
        header("Location: /admin/login.php");
    }
    ?>
                </div>
    </body>
</html>


