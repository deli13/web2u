<html>
    <head>
        <meta charset="utf8">
        <title>Контент</title>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/main.css">
        <script   src="js/jquery.min.js" ></script>
        <script async src="js/bootstrap.min.js"></script>
        <!-- Подключаем TinyMCE -->
            <script src="tinymce/tinymce.min.js"></script>
            <script>tinymce.init({
            	language: 'ru', 
                force_p_newlines : false,
                forced_root_block : false,
                selector:'textarea',
                plugins: ['code autolink link table media jbimages'],
                theme_advanced_buttons1 : "jbimages",
                    });</script>
  <!-- Усё подключили -->
    </head> 
    <?php
require_once 'asset/function.php';
require_once 'model/connect.php';
require_once 'model/dbquery.php';
session();
//$index="Location: /admin/index.php";
//if ($_SESSION["role"]!="admin"){
 //   header($index);
//}

$table='t_content';
$table_publication='t_publication';
$conn=new dbquery($connect, $table);

if (isset($_GET['category'])){
	$table_category='t_category';
	$conn_cat=new dbquery($connect, $table_category);
	$id_cat=$_GET['category'];
	$query_category=$conn_cat->select('id_category='.$id_cat);
	$category=$query_category[0]['name_category'];
}
?>
    <body>
        <?php    include './top.php';?>
        <div class="row">
            <?php include './left_menu.php';?>
            <div class="col-md-10">
                <form method="GET" class="navbar-form" role="search">
                    <input type="text" name="search" class="form-control" style="width:60%" placeholder="Поиск">
                    <input type="submit" class="form-control" value="Найти">
                </form>
                <div class="table_div">
                    <table class="table">
                            <thead>
                                <th>id</th>
                                <th>Название</th>
                                <th>Текст</th>
                                <th>Изображение</th>
                                <th>Публикация</th>
                                <th>Создано</th>
                                <th>Изменено</th>
                                <th>Действие</th>
                            </thead>
                                <tbody>
                                    <?php 
                                    $where="(1=1)";
                                    if (isset($_GET['search'])){
                                        $where=($_GET['search']!="")?"name_content like '%".filter_input(INPUT_GET, 'search', FILTER_SANITIZE_STRING)."%'":"(1=1)";
                                    }
                                    if (isset($_GET['public'])){
                                        $public=$_GET['public'];
                                        $query=$conn->selectJoin($table_publication, 'id_publication', 't_content.id_publication='.$public." and ".$where);
                                    } else {
                                        $query=$conn->selectJoin($table_publication, 'id_publication', $where);
                                    }
                                    foreach( $query as $row){
                                        echo "<tr>";
                                        echo "<td>{$row['id_content']}</td>";
                                        echo "<td>{$row['name_content']}</td>";
                                        echo "<td>".str200($row['text_content'])."</td>";
                                        echo "<td><img height=100 width=100 src='{$row['img_content']}'></td>";
                                        echo "<td>{$row['name_publication']}</td>";
                                        echo "<td>".dateNorm($row['created_content'])."</td>";
                                        echo "<td>".dateNorm($row['updated_content'])."</td>";
                                        echo "<td>"
                                        . "<form name='update'>"
                                        . "<input name='id' value='{$row['id_content']}' style='display:none'>"
                                        . "<input type='submit' name='update' class='btn btn-success' value='Изменить'></form>"
                                        . "<form action='/admin/controller/content-controller.php' name='delete' method='POST'>"
                                        . "<input name='id' value='{$row['id_content']}' style='display:none'>"
                                        . "<input type='submit' name='delete' class='btn btn-danger' value='Удалить'></form>"
                                        ."</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>
                    </table>
                </div>
                <p class="lead">Редактирование контента</p>
                <div class="form-group">
                    <form method="POST" action="/admin/controller/content-controller.php" enctype="multipart/form-data">
                        <input name="id" value="" id="id" style="display: none">
                        <?php if ((isset($_GET['public'])) && ($category=='Портфолио')){
                            ?>
                        <select name="name" id="name" class="form-control">
                            <option value="Проектирование и Техническое задание">Проектирование и Техническое задание</option>
                            <option value="Дизайн">Дизайн</option>
                            <option value="Технологии">Технологии</option>
                            <option value="Наполнение сайта информацией">Наполнение сайта информацией</option>
                            <option value="с">Тестирование сайта</option>
                        </select>
                        <?php
                        } else { ?>
                        <input name="name" id="name" class="form-control" placeholder="Название"></br> <?php } ?>
                        <p class="lead">Текст контента</p>
                        <textarea name="text" id="text" class="form-control" rows="15" placeholder="текст"></textarea><br>
                        <p class="lead">Изображение</p>
                        <input type="file" name="file" accept="image/*"><img src="" id="img"><br>
                        <p class="lead">Логотип</p>
                        <input type="file" name="logo" accept="image/*"><img src="" id="logo"><br>
                        <div class="input-group">
                            <span class="input-group-addon">Цена</span>
                            <input class="form-control" name="price" type="number" id="price">
                        </div><br/>
                        <p class="lead">Выберите публикацию</p>
                        <button type="button" class="btn" data-toggle="collapse" data-target="#collapse">Привязка к публикации</button>
                       <br/><br/>
                       <div id="collapse" class="collapse">
                           <select class="form-control" id="id_pub" name="id_pub">
                            <option selected disabled>Выберите публикацию</option>
                              <?php
                            $conn_pub=new dbquery($connect, $table_publication);
                            $query=$conn_pub->selectColumn('id_publication, name_publication');
                            foreach ($query as $row) {
                                    if ($_GET['public']==$row['id_publication']){
                                       echo "<option selected value='".$row['id_publication']."'>".$row['name_publication']."</option>"; 
                                       continue;
                                    }
                                    echo "<option value='".$row['id_publication']."'>".$row['name_publication']."</option>";}
                            
                            unset($conn_pub);
                            ?>
                           </select><br/>
                       </div>
                        <input type="submit" class="form-control btn-primary" value="Сохранить">
                    </form>
                </div>
            </div>
        </div>
        <script>
                $('form[name=update]').submit(function(){
            form=$(this);
            $.ajax({
                type: 'post',
                url: '/admin/controller/content-controller.php',
                dataType: 'json',
                data: form.serialize()+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                        $('#id').val(req['id']);
                        $('#name').val(req['name']);
                        $('#text').text(req['text']);
                        $('#img').attr("src", req['img']);
                        $('#img').attr("height", '200');
                        $('#id_pub [value="'+req['id_pub']+'"]').attr("selected", "selected");
                        $("#price").val(req["price"]);
                        $('#logo').attr("src", req['logo']);
                        $('#logo').attr("height", '200');
                        tinyMCE.activeEditor.setContent(req['text']);
                }
            })
            return false})
        $('form[name=delete]').submit(function(){
            var conf=confirm('Вы уверены что хотите удалить запись?');
            if (conf==true){
                return true;
            } else{
                return false;
            }
        })
        
        <?php if (isset($_GET['id'])): ?>
        <?php $request=explode("?",$_SERVER['REQUEST_URI']);
        $request=$request[0];
        $id= filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
        ?>   
        document.addEventListener("DOMContentLoaded", function(){
            $.ajax({
                type: 'post',
                url: '/admin/controller/content-controller.php',
                dataType: 'json',
                data: "id=<?php echo $id?>"+'&update',
                success: function(data){
                    var req=$.parseJSON=data;
                        $('#id').val(req['id']);
                        $('#name').val(req['name']);
                        $('#text').text(req['text']);
                        $('#img').attr("src", req['img']);
                        $('#img').attr("height", '200');
                        $('#id_pub [value="'+req['id_pub']+'"]').attr("selected", "selected");
                        $("#price").val(req["price"]);
                        $('#logo').attr("src", req['logo']);
                        $('#logo').attr("height", '200');
                        
                        var btn=document.createElement("button")
                        btn.setAttribute("id", "reload");
                        btn.setAttribute("class", "btn");
                        btn.innerHTML="Добавить запись";
                        document.forms[1].appendChild(btn);    
                        btn.addEventListener("click", function(){console.log("click"); window.location.assign("<?php echo $request; ?>")})
                        
                        tinyMCE.activeEditor.setContent(req['text']);
                }
            })
            })
            <?php endif; ?> 
        
        </script>
    </body>
</html>