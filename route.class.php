<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of route
 *
 * @author death
 */
class SimpleRoute {
    private $route_file; //файл маршрутов
    private $base;




    public function __construct($file_url, $base) {
        $json=file_get_contents($file_url);
        $this->route_file=  json_decode($json, true);
        //print_r($this->route_file);
        $this->base=$base;
    }
    
    public function get($url){
        $url_arr=  explode("/", $url);
        if (($url_arr[0]=="") && ($url_arr[1]=="")){
            $path=$this->route_file["/"];
            return $this->base.$path;
        } else
        if (($url_arr[1]!="") && (!isset($url_arr[2]))){
            $get=strripos($url_arr[1], '?'); //Обработка GET запроса
            if ($get!=false){
               $url_get=explode("?", $url_arr[1]);
               $url_arr[1]=$url_get[0];
            }
            //Проводим роут
            $path=(isset($this->route_file["/".$url_arr[1]]))?$this->route_file["/".$url_arr[1]]:$this->route_file["404"];
            return $this->base.$path;
        } else
        if (($url_arr[1]!="") && (isset($url_arr[2]))){
            $path=(isset($this->route_file["/".$url_arr[1]."/*"]))?$this->route_file["/".$url_arr[1]."/*"]:$this->route_file["404"];
            return $this->base.$path;
        }
        else {
            $path=$this->route_file["404"];
            return $this->base.$path;
        }
    }
    
}
