drop database w2you_site;
create database w2you_site CHARACTER SET utf8 COLLATE utf8_unicode_ci;
use w2you_site;



create table t_template(id_template int auto_increment primary key, name_template varchar(200) unique, path_template varchar(2000)) CHARACTER SET utf8;

create table t_category (id_category int primary key auto_increment, name_category varchar(200) not null unique,description_category text,
created_category timestamp default current_timestamp, id_template int, updated_category timestamp default current_timestamp, title_category varchar(2000), descr_category varchar(2000), keywords_category varchar(2000),
foreign key(id_template) references t_template(id_template)) CHARACTER SET utf8; -- таблица категорий услуги, новости, и.т.п.

create table t_brand (id_brand int primary key auto_increment, name_brand varchar(2000) not null, description_brand text, img_brand varchar(2000),
created_brand timestamp default current_timestamp, updated_brand timestamp default current_timestamp, title_brand varchar(2000), descr_brand varchar(2000), keywords_brand varchar(2000)) CHARACTER SET utf8; -- Таблица с инфо о cms для универсальности названа brand

create table t_img(id_img int primary key auto_increment, img_desktop varchar(2000), img_tablet_vert varchar(2000), img_tablet_horiz varchar(2000),
img_phone_vert varchar(2000), img_phone_horiz varchar(2000)) CHARACTER SET utf8; -- таблица с картиночками

create table t_publication(id_publication int primary key auto_increment, name_publication varchar(200) not null unique, description text,
id_category int, id_brand int, created_publication timestamp default current_timestamp, updated_publication timestamp default current_timestamp, otziv_publication varchar(2000),
id_img int unique, price int, cpu varchar(2000), title_publication varchar(2000), descr_publication varchar(2000), keywords_publication varchar(2000),
foreign key(id_category) references t_category(id_category) on delete restrict,
foreign key(id_brand) references t_brand(id_brand) on delete restrict,
foreign key(id_img) references t_img(id_img)) CHARACTER SET utf8; -- Таблица с инфо о продукте(сайте)

create table t_content (id_content int primary key auto_increment, name_content varchar(2000), text_content text, img_content varchar(2000),
created_content timestamp default current_timestamp, updated_content timestamp default current_timestamp, id_publication int,
foreign key(id_publication) references t_publication(id_publication)) CHARACTER SET utf8; -- таблица с контентом

create table t_news(id_news int primary key auto_increment, name_news varchar(2000), id_content int, created_news timestamp default current_timestamp,
updated_news timestamp default current_timestamp, cpu varchar(2000), title_news varchar(2000), descr_news varchar(2000), keywords_news varchar(2000),
foreign key(id_content) references t_content(id_content) on delete restrict) CHARACTER SET utf8; -- таблица с новостями (новости так то у всех есть=^____^=)

create table t_hashtag(id_hashtag int auto_increment primary key, hashtag varchar(100)) CHARACTER SET utf8; -- хэштеги

create table t_hashtag_publication(id_hashtag_publication int primary key auto_increment, id_hashtag int, id_publication int,
foreign key (id_hashtag) references t_hashtag(id_hashtag),
foreign key (id_publication) references t_publication(id_publication)) CHARACTER SET utf8; -- связь хэштегов с публикациями

create table t_user(id_user int auto_increment primary key, login_user varchar(100) not null unique, role varchar(100), password_user varchar(40) not null, created_user timestamp default current_timestamp) CHARACTER SET utf8;

alter table t_publication add column viv_publication int;
alter table t_publication add column dop_uslugi_publication varchar(2000) default null;
alter table t_content add column logo_content varchar(2000);
alter table t_content add column price_content varchar(2000);









